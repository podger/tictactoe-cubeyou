(function (angular) {

    function GameLog() {
        var service = this;

        this.texts = "";

        this.playerTurn = function() {
            // this.texts = "It's your turn! Make a move!";
        }

        this.comTurn = function() {
            // this.texts = "It's COM turn. Wait..";
        }

        this.youWin = function() {
            this.texts = "YOU WIN";
        }

        this.comWin = function() {
            this.texts = "YOU LOSE";
        }

        this.tie = function() {
            this.texts = "TIE!";
        }

        this.write = function(text) {
            this.texts = text;
        }

        this.clear = function () {
            this.texts = "";
        }

        return service;
    }

    angular.module('gameApp').factory('$gameLog', [GameLog])

})(window.angular);