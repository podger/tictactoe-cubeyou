(function (angular) {

    function GameManager($gameState, $gameLog) {
        var privateSrv = this;

        
        this.doRandomMove = function () {
            var state = $gameState.getState();

            if ($gameState.isGameOn()) {
                var pos = 0;
                do {
                    pos = Math.floor((Math.random()) * 10) % (state.allPositions.length - 1);
                } while (!this.putSign(state.allPositions[pos]));
            }
        }

        this.nextTurn = function () {
            var results = $gameState.updateGameState();
            if (results) {
                this.handleResults(results);
                return;
            }

            if ($gameState.isPLturn()) {
                $gameState.updateTurn('COM');
                $gameLog.comTurn();

                this.doRandomMove();

            } else {
                $gameState.updateTurn('PL');
                $gameLog.playerTurn();
            }
        }

        this.handleResults = function (res) {
            switch (res) {
                case "COM":
                    $gameLog.comWin();
                    break;
                case "PL":
                    $gameLog.youWin();
                    break;
                case "TIE":
                    $gameLog.tie();
                    break;
                default:
                    break;
            }
        }

        this.canPositionSign = function (code) {
            if (!code)
                return false;

            return !$gameState.isPositionOccupied(code);
        }

        this.getOccupier = function (code) {
            return $gameState.getOccupier(code);
        }


        this.reset = function () {
            $gameState.reset();
            $gameLog.clear();
        }

        this.init = function () {
            $gameState.reset();
        }

        this.putSign = function (code) {
            if (this.canPositionSign(code)) {
                $gameState.updateTable(code);
                this.nextTurn();
                return true;
            }

            return false;
        }

        this.isPlayerTurn = function () {
            return $gameState.isPLturn();
        }

        this.isGameOn = function () {
            return $gameState.isGameOn();
        }

        this.isWinningCell = function (code) {
            return $gameState.getState().winningCombination.filter(function (el) {
                return el === code;
            }).length > 0;
        }

        var publicSrv = {
            isWinningCell: privateSrv.isWinningCell.bind(privateSrv),
            isGameOn: privateSrv.isGameOn.bind(privateSrv),
            isPlayerTurn: privateSrv.isPlayerTurn.bind(privateSrv),
            putSign: privateSrv.putSign.bind(privateSrv),
            init: privateSrv.init.bind(privateSrv),
            getOccupier: privateSrv.getOccupier.bind(privateSrv),
            canPositionSign: privateSrv.canPositionSign.bind(privateSrv),
            reset: privateSrv.reset.bind(privateSrv)
        }

        return publicSrv;
    }

    angular.module('gameApp').factory('$gameManager', ['$gameState', '$gameLog', GameManager])

})(window.angular);