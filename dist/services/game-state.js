(function (angular) {

    function GameState() {
        var service = this;

        this.GameState = function () {
            this.turn = 'PL',
                this.state = 'PLAYING',
                this.allPositions = ["NW", "N", "NE", "W", "C", "E", "SW", "S", "SE"],
                this.userOccupied = [],
                this.comOccupied = [],
                this.winningCombination = []

        };

        this.state = new service.GameState();

        this.winningCombination = [
            ["NW", "N", "NE"],
            ["NW", "W", "SW"],
            ["NW", "C", "SE"],
            ["N", "C", "S"],
            ["NE", "C", "SW"],
            ["NE", "E", "SE"],
            ["W", "C", "E"]
        ];


        this.isWinningCombination = function (combination) {
            let res = this.winningCombination.filter(function (wc) {
                let intersect = combination.filter((function (c) {
                    return wc.indexOf(c) > -1;
                }));

                if (intersect.length === 3) {
                    service.state.winningCombination = intersect;
                    return true;
                } else {
                    return false;
                }

            });


            return !!res.length;
        }

        this.getState = function () {
            return service.state;
        };

        this.isPositionOccupied = function (position) {
            return service.state.userOccupied.indexOf(position) > -1 || service.state.comOccupied.indexOf(position) > -1;
        };

        this.getOccupier = function (position) {
            if (service.state.userOccupied.indexOf(position) > -1) {
                return 'PL';
            } else if (service.state.comOccupied.indexOf(position) > -1) {
                return 'COM';
            } else {
                return false;
            }
        };

        this.isPLturn = function () {
            return service.state.turn === 'PL';
        };

        this.updateTable = function (position) {
            if (service.state.turn === 'PL') {
                service.state.userOccupied.push(position);
            } else if (service.state.turn === 'COM') {
                service.state.comOccupied.push(position);
            }
        };

        this.updateGameState = function () {
            if (service.isWinningCombination(service.state.userOccupied)) {
                service.state.state = "NOTPLAYING";
                return "PL";
            }
            if (service.isWinningCombination(service.state.comOccupied)) {
                service.state.state = "NOTPLAYING";
                return "COM"
            }

            if ((service.state.userOccupied.length + service.state.comOccupied.length) === service.state.allPositions.length) {
                service.state.state = "NOTPLAYING";
                return "TIE"
            }

            return null;

        };

        this.isGameOn = function () {
            return service.state.state === "PLAYING";
        };

        this.updateTurn = function (pl) {
            service.state.turn = pl;
        };

        this.reset = function () {
            service.state = new service.GameState();
        };

        var publicInterface = {
            getState: service.getState.bind(service),

            isPositionOccupied: service.isPositionOccupied.bind(service),

            getOccupier: service.getOccupier.bind(service),

            isPLturn: service.isPLturn.bind(service),

            updateTable: service.updateTable.bind(service),

            updateGameState: service.updateGameState.bind(service),

            isGameOn: service.isGameOn.bind(service),

            isGameOn: service.isGameOn.bind(service),

            updateTurn: service.updateTurn.bind(service),

            reset: service.reset.bind(service)
        }

        return publicInterface;
    }

    angular.module('gameApp').factory('$gameState', [GameState]);

})(window.angular);