(function(angular) {

    function GameButtonController($gameManager) {
        // var ctrl = this;
        this.gm = $gameManager;

        this.getSign = function () {
            let occupier = $gameManager.getOccupier(this.code);
            if (occupier == 'COM') {
                return "X";
            }
            if (occupier == 'PL') {
                return "O";
            }
            return "";
        }

    }

    angular.module('gameApp').component('gameButton', {
        templateUrl: './components/game-button/game-button.html',
        controller: GameButtonController, 
        bindings: {
            code: "@"
        }
    });

})(window.angular);