(function(angular) {

    function GameTableController($gameLog, $gameManager) {
        var ctrl = this;
    
        this.gl = $gameLog;

        this.resetGame = function() {
            $gameManager.reset();
        }

    }

    angular.module('gameApp').component('gameTable', {
        templateUrl: './components/game-table/game-table.html',
        controller: GameTableController, 
    });

})(window.angular);