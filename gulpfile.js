var gulp = require("gulp");
var gls = require('gulp-live-server');

var sourceFiles = [
    './src/**/*',
];

var vendors = [
    './node_modules/**/angular.js',
]

gulp.task('copy-src', function () {
    return gulp
        .src(sourceFiles, {
            base: './src',

        })
        .pipe(gulp.dest('dist'))
});

gulp.task('copy-vendor', function () {
    return gulp
        .src(vendors)
        .pipe(gulp.dest('dist/node_modules/'))
})

gulp.task("watch", function () {
    gulp.watch(sourceFiles, ['copy-src']);
});

gulp.task('serve', function () {
    var server = gls.static('dist', 8888);

    server.start();

    //use gulp.watch to trigger server actions(notify, start or stop) 
    gulp.watch(['./dist/**/*.js', './dist/**/*.html'], function (file) {
        server.notify.apply(server, [file]);
    });
});

gulp.task('default', ['watch', 'copy-src', 'copy-vendor','serve']);